<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::view('/', 'home')->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/exists', [\App\Http\Controllers\ApiController::class, 'index']);



Route::view('services', 'services')->name('services');
Route::view('calendar', '/calendar/calendar');
Route::get('admin/users', [\App\Http\Controllers\AdminController::class, 'users'])->name('users')->middleware('admin');
Route::get('admin/doctors', [\App\Http\Controllers\AdminController::class, 'doctors'])->name('doctors')->middleware('admin');
Route::get('admin/specialties', [\App\Http\Controllers\AdminController::class, 'specialties'])->name('specialties')->middleware('admin');
Route::get('admin/appointments', [\App\Http\Controllers\AdminController::class, 'appointments'])->name('appointments')->middleware('admin');

//Specialists Team
Route::get('doctors', [\App\Http\Controllers\DoctorsController::class, 'index'])->name('doctors.index');
Route::get('doctors/create', [\App\Http\Controllers\DoctorsController::class, 'create'])->name('doctors.create')->middleware('manager');
Route::post('doctors', [\App\Http\Controllers\DoctorsController::class, 'store'])->name('doctors.store');
Route::get('doctors/{doctor}', [\App\Http\Controllers\DoctorsController::class, 'show'])->name('doctors.show');
Route::get('doctors/{doctor}/edit', [\App\Http\Controllers\DoctorsController::class, 'edit'])->name('doctors.edit')->middleware('admin');
Route::patch('doctors/{doctor}', [\App\Http\Controllers\DoctorsController::class, 'update'])->name('doctors.update');
Route::delete('doctors/{doctor}', [\App\Http\Controllers\DoctorsController::class, 'destroy'])->name('doctors.destroy');

Auth::routes();
//Contact form
Route::get('/contact', [\App\Http\Controllers\ContactFormController::class, 'create'])->name('contact.create');
Route::post('/contact', [\App\Http\Controllers\ContactFormController::class, 'store'])->name('contact.store');
//Admin dashboard
Route::get('/admin', [\App\Http\Controllers\AdminController::class, 'index'])->name('admin')->middleware('admin');
Route::get('/admin/create', [\App\Http\Controllers\AdminController::class, 'create'])->name('admin.create')->middleware('admin', 'manager');
Route::post('/admin', [\App\Http\Controllers\AdminController::class, 'store'])->name('admin.store')->middleware('admin');
Route::get('/admin/{user}', [\App\Http\Controllers\UserController::class, 'show'])->name('admin.show');
Route::get('/admin/{user}/edit', [\App\Http\Controllers\AdminController::class, 'edit'])->name('admin.edit')->middleware('admin');;
Route::patch('/admin/{user}', [\App\Http\Controllers\AdminController::class, 'update'])->name('admin.update')->middleware('admin');;
Route::delete('/admin/{user}', [\App\Http\Controllers\AdminController::class, 'destroy'])->name('admin.destroy')->middleware('admin');;
//User and Manager dashboard
Route::get('/user', [\App\Http\Controllers\UserController::class, 'index'])->name('user')->middleware('user');
Route::get('/manager', [\App\Http\Controllers\ManagerController::class, 'index'])->name('manager')->middleware('manager');


Route::get('/appointments', [\App\Http\Controllers\SpecController::class, 'index'])->name('spec.index');
Route::get('/specialisation', [\App\Http\Controllers\SpecController::class, 'create'])->name('spec.create')->middleware('admin');;
Route::post('/specialisation', [\App\Http\Controllers\SpecController::class, 'store'])->name('spec.store')->middleware('admin');;
Route::get('/specialisation/{spec}', [\App\Http\Controllers\SpecController::class, 'show'])->name('spec.show');
Route::get('/specialisation/{spec}/edit', [\App\Http\Controllers\SpecController::class, 'edit'])->name('spec.edit')->middleware('admin');;
Route::patch('/specialisation/{spec}', [\App\Http\Controllers\SpecController::class, 'update'])->name('spec.update')->middleware('admin');;
Route::delete('/appointments/{spec}', [\App\Http\Controllers\SpecController::class, 'destroy'])->name('spec.destroy')->middleware('admin');;
Route::get('/appointment/{spec}', [\App\Http\Controllers\AppointmentsController::class, 'index'])->name('appointment');
//Route::get('/appointment/{spec}', [\App\Http\Controllers\AppointmentsController::class, 'create'])->name('appointment.create');
Route::post('/appointment', [\App\Http\Controllers\ApiController::class, 'store'])->name('appointment.store');
Route::get('/appointments/show', [\App\Http\Controllers\AppointmentsController::class, 'show'])->name('appointment.show');


Route::get('/demo', [\App\Http\Controllers\DemoController::class, 'index'])->name('demoRoute');


//[\App\Http\Controllers\DemoController::class, 'demo']
//    $doctor = \App\Models\Doctor::first();

//    $created_at=\Carbon\Carbon::parse("2018-07-23 12:15:43");
//    $expired=$created_at->addMinutes(30);
//
//    dd($expired);


//Route::get('/axios', function () {
//    return view('example.axios');
//});
//
//Route::get('/axios/get', function (Illuminate\Http\Request $request) {
////    if (!$request->has("email")) {
////        return response()->json(["message" => "parameters missing from request"], 400);
////    }
//
////    $email = $request->get('email');
//
//    if (empty($doctor = App\Models\Doctor::query()->first()).$day=App\Models\Day::all()) {
//        return response()->json(["message" => "no doctor found for this email"], 404);
//    }
//    return response()->json(["message" => "Found doctor!", "doctor" => $doctor]);
//});



