<div class="container">
    <div class="form-group">
        <label class="form-label" for="name">Name</label>
        <div class="text-danger">{{ $errors->first('name') }}</div>
        <input type="text" class="form-control" id="name" name="name" placeholder="Your name" tabindex="1"
               value="{{ old('name') ?? $spec->name}}" required>
    </div>
    <div class="form-group d-flex flex-column">
        <label for="image">Profile Image</label>
        <input type="file" name="image">
    </div>
</div>

@csrf
