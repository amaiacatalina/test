@extends('layouts.app')
@section('content')
    <div class="team-wrap">
        @if($spec->image)
            <div class="team-profile">
                <img src=" {{ asset('storage/' . $spec->image) }}" alt="">
            </div>
        @endif

        <div class="team-content">
            <h5>{{ $spec->name }}</h5>
            <form class="form" action="{{ route('spec.destroy', ['spec'=>$spec]) }}" method="POST" enctype="multipart/form-data">
                @method('DELETE')
                <a href="{{ route('spec.edit', ['spec'=>$spec])}}" class="btn btn-primary py-0">Edit</a>
                <button type="submit" class="btn btn-danger py-0">Delete</button>
                <a href="{{ route('spec.index')}}" class="btn btn-success py-0">Back</a>
                @csrf
            </form>
        </div>
    </div>
@endsection
