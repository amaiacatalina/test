@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            @foreach($specs as $spec)
                <div class="col-4 my-2">
                    <div class="card" style="width: 18rem;">
                        @if($spec->image)
                            <img class="card-img-top" src=" {{ asset('storage/' . $spec->image) }}" alt="">
                        @endif
                        <div class="text-center card-body">
                            <a class="card-title text-decoration-none text-dark"
                               href="{{route('spec.show',['spec'=>$spec])}}">{{$spec->name}} </a>
                            <p class="card-text"></p>
                            <a href="{{route('appointment', ['spec'=>$spec])}}" class="btn btn-success py-0">Get an
                                appointment</a>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>

    @push('js')
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            console.log('heyy')
            let userId = @json(auth()->id());
            console.log(userId);
            Echo.private('user.' + userId)
            .listen('.dateEvent', (e) => {
                Swal.fire({
                    title: 'Success!',
                    text: 'Appointment complete!',
                    icon: 'info',
                    confirmButtonText: 'Cool'
                })
            })

        </script>
    @endpush
@endsection
