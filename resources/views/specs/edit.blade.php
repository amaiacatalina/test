@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center py-4">
                <h2>Edit speciality</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 add">
                <form class="form" action="{{ route('spec.update', ['spec'=>$spec]) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @include('specs.form')
                    <button type="submit" class="btn btn-primary">Edit</button>
                    <div class="text-center">
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
@endsection
