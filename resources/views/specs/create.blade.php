@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center py-4">
            <h2>Add a new specialisation</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 m-auto add">
            <form class="form" action="{{ route('spec.store') }}" method="POST" enctype="multipart/form-data">
                @include('specs.form')
                <div class="text-center">
                    <a class="btn btn-danger" href="{{route('admin')}}">Cancel</a>
                    <button type="submit" class="btn btn-primary">Add specialisation</button>

                </div>
            </form>
        </div>
    </div>
</div>

@endsection
