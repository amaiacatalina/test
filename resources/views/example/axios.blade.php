@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
{{--            <input type="text" name="email">--}}
            <button class="btn btn-success button_id">Button</button>
            <div class="doctor-details">

            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js" integrity="sha512-u9akINsQsAkG9xjc1cnGF4zw5TFDwkxuc9vUp5dltDWYCSmyd0meygbvgXrlc/z7/o4a19Fb5V0OUE58J7dcyw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $('.button_id').on("click", function (e) {
        // let email = $('input[name="email"]').val();

        axios.get('/axios/get')
            .then(function (response) {
                $('.doctor-details').append(response.data.doctor.day_id);
            })
            .catch(function (error) {
                console.log(error);
            });
    });
</script>
@endpush
