@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row flex-nowrap">
            <main class="col">
                @foreach($users as $user)
                    @if($user->id === \Illuminate\Support\Facades\Auth::id())
                <section style="background-color: #eee;">
                    <div class="container py-5">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card mb-4">
                                    <div class="card-body text-center">
                                        <h5 class="my-3">{{\Illuminate\Support\Facades\Auth::user()->name}}</h5>
                                        <p class="text-muted mb-1">{{\Illuminate\Support\Facades\Auth::user()->email}}</p>
                                        <div class="d-flex justify-content-center mb-2">
                                            <button type="button" class="btn btn-outline-primary ms-1">Edit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4 mb-lg-0">
                                    <div class="card-body p-0">
                                        <ul class="list-group list-group-flush rounded-3">
                                            <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                                                <i class="fas fa-globe fa-lg text-warning"></i>
                                                <p class="mb-0">https://mdbootstrap.com</p>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                                                <i class="fab fa-github fa-lg" style="color: #333333;"></i>
                                                <p class="mb-0">mdbootstrap</p>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                                                <i class="fab fa-twitter fa-lg" style="color: #55acee;"></i>
                                                <p class="mb-0">@mdbootstrap</p>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                                                <i class="fab fa-instagram fa-lg" style="color: #ac2bac;"></i>
                                                <p class="mb-0">mdbootstrap</p>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                                                <i class="fab fa-facebook-f fa-lg" style="color: #3b5998;"></i>
                                                <p class="mb-0">mdbootstrap</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="card">
                                            <div class="card-body ">

                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>Doctor name</th>
                                                        <th>Description</th>
                                                        <th>Date</th>
                                                        <th>User</th>
                                                    </tr>
                                                    <tr>
                                                        @foreach($apps as $app)
                                                            @if($app->user_id === \Illuminate\Support\Facades\Auth::id() )
                                                                <td>{{$app->doctor->name}}</td>
                                                                <td>{{$app->description}}</td>
                                                                <td>{{$app->date}}</td>
                                                                <td>{{$app->user->name}}</td>
                                                            @endif
                                                    </tr>
                                                    @endforeach
                                                </table>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                    @endif
                @endforeach

            </main>
        </div>
    </div>


    </div>
@endsection
