
@foreach($doctors as $doctor)
    <div class="team-wrap">
        @if($doctor->image)
            <div class="team-profile">
                <img src=" {{ asset('storage/' . $doctor->image) }}" alt="">
            </div>
        @elseif($doctor->gender=='male')
            <div class="team-profile">
                <img alt="" src="{{ asset('storage/teams/male.jpeg' )}}"
                     style="width: 280px; height: 350px">
            </div>
        @elseif($doctor->gender=='female')
            <div class="team-profile">
                <img alt="" src="{{ asset('storage/teams/female.jpeg' )}}"
                     style="width: 280px; height: 350px">
            </div>
        @endif

        <div class="team-content">
            <h5>{{ $doctor->name }}</h5>
            <div class="text-sm fg-grey a"><h5>{{$doctor->spec->name}}</h5></div>
            <div class="text-sm fg-grey a">Phone: <span>{{$doctor->phone}}</span></div>
            <div class="text-sm fg-grey a">Work schedule:<br><span></span></div>
            <a href="/doctors/{{ $doctor->id }}" class="btn btn-success py-0">More details</a>
            <div class="social-button">
                <a href="#"><span class="mai-logo-facebook-messenger"></span></a>
                <a href="#"><span class="mai-mail"></span></a>
            </div>
        </div>

    </div>
@endforeach
