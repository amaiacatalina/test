@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center py-4">
                <h2>Add a new specialist</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 add m-auto ">
                <form class="form" action="{{ route('doctors.store', ['doctor'=>$doctor]) }}" method="POST" enctype="multipart/form-data">
                    @include('doctors.form')
                    <div class="text-center">
                        <a class="btn btn-danger" href="{{route('admin')}}">Cancel</a>
                        <button type="submit" class="btn btn-primary">Add to the team</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

