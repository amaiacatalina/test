@extends('layouts.app')
@section('content')
    <div class="team-wrap">
        @if($doctor->image)
            <div class="team-profile">
                <img src=" {{ asset('storage/' . $doctor->image) }}" alt="">
            </div>
        @elseif($doctor->gender=='male')
            <div class="team-profile">
                <img alt="" src="{{ asset('storage/teams/male.jpeg' )}}"
                     style="width: 280px; height: 350px">
            </div>
        @elseif($doctor->gender=='female')
            <div class="team-profile">
                <img alt="" src="{{ asset('storage/teams/female.jpeg' )}}"
                     style="width: 280px; height: 350px">
            </div>
        @endif

        <div class="team-content">
            <h5>{{ $doctor->name }}</h5>
            <div class="text-sm fg-grey a">{{$doctor->spec->name}}</div>
            <div class="text-sm fg-grey a">Phone: <span>{{$doctor->phone}}</span></div>
            <div class="text-sm fg-grey a">Work schedule: <br><span></span></div>

            <form class="form" action="{{ route('doctors.destroy', ['doctor'=>$doctor]) }}" method="POSt">
                @method('DELETE')
                <a href="{{ route('doctors.edit', ['doctor'=>$doctor])}}" class="btn btn-primary py-0">Edit</a>
                <button type="submit" class="btn btn-danger py-0">Delete</button>

                @csrf
            </form>
            <div class="social-button">
                <a href="#"><span class="mai-logo-facebook-messenger"></span></a>
                <a href="#"><span class="mai-mail"></span></a>
            </div>
        </div>
    </div>
@endsection
