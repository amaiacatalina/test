@extends('layouts.app')
@section('content')
    <body>

    <!-- Back to top button -->
    <div class="back-to-top"></div>

    <header>
        {{--@include('nav') <!-- .navbar -->--}}



    <main>
        @include('admin.about')
        <div class="page-section">
            <div class="container">
                <div class="text-center">
                    <div class="subhead">Our Teams</div>
                    <h2 class="title-section">The Expert Team on ReveMED</h2>
                </div>

                <div class="owl-carousel team-carousel mt-5">
                    @include('doctors.team')

                </div>
            </div> <!-- .container -->
        </div> <!-- .page-section -->

    </main>



    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIA_zqjFMsJM_sxP9-6Pde5vVCTyJmUHM&callback=initMap"></script> -->

    </body>
    </html>



@endsection
