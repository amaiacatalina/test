{{--                    ADD PHOTO    --}}
<div class="form-group">
    <label class="form-label" for="name">Name</label>
    <div class="text-danger">{{ $errors->first('name') }}</div>
    <input type="text" class="form-control" id="name" name="name" placeholder="Your name" tabindex="1"
           value="{{ old('name') ?? \Illuminate\Support\Facades\Auth::user()->name}}" required>
</div>
<div class="form-group">
    <label class="form-label" for="email">Email</label>
    <div class="text-danger">{{ $errors->first('email') }}</div>
    <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" tabindex="2"
           value="{{ old('email') ?? \Illuminate\Support\Facades\Auth::user()->email}}" required>
</div>
<div class="form-group">
    <label class="form-label" for="subject">Phone</label>
    <div class="text-danger">{{ $errors->first('phone') }}</div>
    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" tabindex="3"
           value="{{ old('phone') ?? $doctor->phone}}">
</div>
<div class="form-group">
    <label class="form-label" for="Specialisation">Specialisation</label>
    <div class="text-danger">{{ $errors->first('spec_id') }}</div>
    <select name="spec_id" class="form-control" id="specs">
        {{--                           <option selected disabled>Open this select menu</option>--}}
        @foreach($specs as $spec)
            <option name="spec_id"
                    value="{{$spec->id}}"
                    @if(old('spec_id') == $doctor['spec_id'] || $doctor['spec_id'] == $doctor->spec_id) selected @endif>{{$spec->name}}</option>
        @endforeach

    </select>
    {{--        <label class="form-label py-2" for="day_id[]">Availability</label>--}}
    {{--        <div class="text-danger">{{ $errors->first('day_id') }}</div>--}}
    {{--        @foreach($days as $day)--}}
    {{--            <div class="custom-control custom-checkbox">--}}
    {{--                <input type="checkbox" name="day_id[]" class="custom-control-input" value={{ $day->name }} id="{{ $day->name }}">--}}
    {{--                <label class="custom-control-label" for="{{ $day->name }}">{{ $day->name }}</label>--}}
    {{--            </div>--}}
    {{--        @endforeach--}}
    {{--    </div>--}}
</div>
<div class="form-group">
    <label for="input_from">From</label>
    <input type="text" class="form-control" name="startdate" id="input_from" placeholder="Start Date"
           value="{{ old('startdate') }}">
</div>

<div class="form-group">
    <label for="input_to">To</label>
    <input type="text" class="form-control" name="enddate" id="input_to" placeholder="End Date"
           value="{{ old('enddate') }}">
</div>
<div class="text-danger">{{ $errors->first('startdate') }}</div>


<div class="form-group">
    @if(Route::is('doctors.create'))
        <div id="append">
            <input type="hidden" value="{{$user}}" name="user_id">
        </div>
        @else
        <div id="append">
            <input type="hidden" value="{{$user}}" name="user_id">
        </div>

    @endif
    <label for="gender">Gender</label>
    <div class="form-check">
        <input class="form-check-input @error('gender') is-invalid @enderror" type="radio" name="gender"
               id="genderF"
               value="female" @if(old('gender') == 'female' || $doctor->gender == 'female') checked @endif>
        <label class="form-check-label" for="genderF">Female</label>
    </div>
    <div class="form-check">
        <input class="form-check-input @error('gender') is-invalid @enderror" type="radio" name="gender"
               id="genderM"
               value="male" @if(old('gender') == 'male'|| $doctor->gender == 'male') checked @endif>
        <label class="form-check-label" for="genderM">Male</label>
    </div>
    <div class="text-danger">{{ $errors->first('gender') }}</div>
</div>
<div class="form-group d-flex flex-column">
    <label for="image">Profile Image</label>
    <input type="file" name="image"
           value="@if(old('image') == $doctor['image'] || $doctor['image'] == $doctor->image)  @endif">
    <div>{{ $errors->first('day_id') }}</div>
</div>

@csrf
