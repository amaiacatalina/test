<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-success" id="exampleModalLabel">Appointment details</h5>
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{route('appointment.store')}}" method="POST" id="app-form">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" value=""
                               placeholder="name@example.com">
                        {{ $errors->first('email') }}
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Short description</label>
                        <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        {{ $errors->first('description') }}
                    </div>
                    @csrf
                    <div id="append">
                        <input type="hidden" value="{{$user}}" name="user_id">

                    </div>
                    <div class="form-group">
                        <label for="interval">Select appointment day</label>
                        <input autocomplete="off" type="text" name="date" id="datepicker">
                        {{ $errors->first('date') }}
                    </div>

                    <div class="modal-footer">
                        {{--                        <button type="button" class="btn btn-secondary close-bm" data-dismiss="modal">Close</button>--}}
                        <button type="submit" class="btn btn-primary submit">Save changes</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@push('js')
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

    <script>
        $('.btn-appointment').on("click", (e) => {
            let doctor = $(e.currentTarget).data('doctor');
            let startDate = new Date($(e.currentTarget).data('start'));
            let endDate = new Date($(e.currentTarget).data('end'));
            var array = $(e.currentTarget).data('exists');
            function appointedDays(date) {
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                return [array.indexOf(string) === -1]
            }
            function noWeekendsOrHolidays(date) {
                var noWeekend = jQuery.datepicker.noWeekends(date);
                return noWeekend[0] ? appointedDays(date) : noWeekend;
            }
            $('#datepicker').datepicker({
                minDate: startDate,
                maxDate: endDate,
                dateFormat: 'y-mm-dd',
                beforeShowDay:noWeekendsOrHolidays,
            });
            $('#append').append('<input type="hidden" value="' + doctor + '" name="doctor_id">');
            $('#exampleModal').modal('show');
        });
        $("#exampleModal").on("hidden.bs.modal", function () {
            $(this).removeData();
            $("#datepicker").val('').datepicker("destroy");

        });

    </script>
@endpush
