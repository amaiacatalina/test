@extends('layouts.app')
@push('css')
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
@endpush
@section('content')
    <div class="container">
        <div class="owl-carousel team-carousel mt-5">
            @foreach($doctors as $doctor)
                <div class="team-wrap">
                    @if($doctor->image)
                        <div class="team-profile">
                            <img src=" {{ asset('storage/' . $doctor->image) }}" alt="">
                        </div>
                    @elseif($doctor->gender === 'male')
                        <div class="team-profile">
                            <img alt="" src="{{ asset('storage/teams/male.jpeg' )}}"
                                 style="width: 280px; height: 350px">
                        </div>
                    @elseif($doctor->gender === 'female')
                        <div class="team-profile">
                            <img alt="" src="{{ asset('storage/teams/female.jpeg' )}}"
                                 style="width: 280px; height: 350px">
                        </div>
                    @endif

                    <div class="team-content">
                        <h5>{{ $doctor->name }}</h5>
                        <div class="text-sm fg-grey a"><h5>{{$doctor->spec->name}}</h5></div>
                        <div class="text-sm fg-grey a"><h6>Phone:</h6><span>{{$doctor->phone}}</span></div>
                        <div class="text-sm fg-grey a"><h6>Available for appointments:</h6>{{$doctor->startdate}}-{{$doctor->enddate}}</span></div>
                        {{--                        <a href="#" class="btn btn-success py-0">Get appointment</a>--}}
                    </div>

                        <div class="container text-center m-auto">
                            @if (Auth::check())
                            <button type="button" class="btn btn-success btn-appointment m-auto" data-exists="{{$doctor->dates}}"
                                    data-doctor="{{ $doctor->id }}" data-start="{{ $doctor->startdate }}"
                                    data-end="{{ $doctor->enddate }}" >
                                Get an appointment
                            </button>
                                @endif

                        </div>

                </div>
            @endforeach
        </div>
    </div>
    @include('appointments.create')
@endsection

