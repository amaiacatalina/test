 <table class="table table-hover">
                <tr>
                    <th>Doctor name</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>User</th>
                    <th colspan="2">Approved</th>
                </tr>
                <tr>
                    @foreach($apps as $app)
                        @if($app->doctor->user_id === \Illuminate\Support\Facades\Auth::id() )
                    <td>{{$app->doctor->name}}</td>
                    <td>{{$app->description}}</td>
                    <td>{{$app->date}}</td>
                    <td>{{$app->user->name}}</td>
                    <td><a class="text-success" href="#"><i class="fas fa-check"></i></a></td>
                    <td><a class="text-danger" href="#"><i class="far fa-times-circle"></i></i></a></td>
                        @endif
                </tr>
                @endforeach
            </table>



