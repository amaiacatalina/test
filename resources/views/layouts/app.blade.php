<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"
          integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="copyright" content="MACode ID, https://macodeid.com/">


    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/maicons.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/animate/animate.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/css/owl.carousel.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/fancybox/css/jquery.fancybox.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/theme.css')}}">


    <link rel="stylesheet" href="{{asset('assets2/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets2/css/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets2/css/classic.date.css')}}">
@stack('css')
@yield('css')
    <!-- Bootstrap CSS -->
{{--    <link rel="stylesheet" href="{{asset('assets2/css/bootstrap.min.css')}}">--}}

<!-- Style -->
    <link rel="stylesheet" href="{{asset('assets2/css/style.css')}}">
</head>
<body>
@include('topbar') <!-- .top-bar -->
<div id="app">
    @include('nav')

    <main class="py-4">
        @yield('content')
        @yield('js')
    </main>
</div>
@include('footer')
<script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('assets/vendor/wow/wow.min.js')}}"></script>
<script src="{{asset('assets/vendor/fancybox/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('assets/vendor/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/google-maps.js')}}"></script>
<script src="{{asset('assets/js/theme.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>


<script src="{{asset('assets2/js/popper.min.js')}}"></script>
<script src="{{asset('assets2/js/picker.js')}}"></script>
<script src="{{asset('assets2/js/picker.date.js')}}"></script>

<script src="{{asset('assets2/js/main.js')}}"></script>
<script src="{{asset('assets/vendor/owl-carousel/js/owl.carousel.min.js')}}"></script>
@stack('js')
@include('sweetalert::alert')


</body>
</html>
