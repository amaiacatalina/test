@extends('layouts.app')
@section('content')
    <div class="team-wrap">
    <div class="team-content">
        <h5>Name: {{$user->name}}</h5>
        <div class="text-sm fg-grey a">Email: {{$user->email}}</div>
        @if($user->role=='1')
            <div class="text-sm fg-grey a">Role:Admin</div>
        @endif
        @if($user->role=='2')
            <div class="text-sm fg-grey a">Role: Manager</div>
        @endif
        @if($user->role=='3')
            <div class="text-sm fg-grey a">Role: User</div>

        @endif
        <form class="form" action="{{ route('admin.destroy', ['user'=>$user]) }}" method="POST">
            @method('DELETE')
            <a href="{{ route('admin.edit', ['user'=>$user])}}" class="btn btn-primary py-0">Edit</a>
            <button type="submit" class="btn btn-danger py-0">Delete</button>

            @csrf
        </form>
    </div>
@endsection
