<header>
<div class="page-banner bg-img bg-img-parallax overlay-dark" style="background-image: url(../assets/img/bg_image_3.jpg);">
    <div class="container h-100">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-lg-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-dark bg-transparent justify-content-center py-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> About</li>
                    </ol>
                </nav>
                <h1 class="fg-white text-center">About</h1>
            </div>
        </div>
    </div>
</div> <!-- .page-banner -->
</header>

<div class="page-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 py-3">
                <div class="subhead">About Us</div>
                <h2 class="title-section">We are <span class="fg-primary">Professional Teams</span> to Growth your Business</h2>

                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of</p>
            </div>
            <div class="col-lg-6 py-3">
                <div class="about-img">
                    <img src="../assets/img/about.jpg" alt="">
                </div>
            </div>
        </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->
