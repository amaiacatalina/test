@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center py-4">
                <h2>Edit your profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 m-auto add">
                <form class="form" action="{{ route('admin.update', ['user'=>$user]) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @include('admin.form')
                    <button type="submit" class="btn btn-primary">Edit</button>
                    <div class="text-center">
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
@endsection
