
{{--                    ADD PHOTO    --}}

<div class="form-group">
    <label class="form-label" for="name">Name</label>
    <div class="text-danger">{{ $errors->first('name') }}</div>
    <input type="text" class="form-control" id="name" name="name" placeholder="Your name" tabindex="1"  required value="{{old('name') ?? $user->name}}">
</div>
<div class="form-group">
    <label class="form-label" for="email">Email</label>
    <div class="text-danger">{{ $errors->first('email') }}</div>
    <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" tabindex="2"  required value="{{old('email') ?? $user->email}}">
</div>
<div class="form-group">
    <label for="password">Password</label>
    <div class="text-danger">{{ $errors->first('password') }}</div>
    <input type="password" class="form-control" name="password" id="password" placeholder="password" tabindex="3"  required value="{{old('passworld') ?? $user->password}}">
</div>
<div class="form-group">
    <label class="form-label" for="role">Role</label>
    <div class="text-danger">{{ $errors->first('role') }}</div>
    <select name="role" class="form-control" id="role" value="{{old('passworld') ?? $user->password}}">
        {{--                           <option selected disabled>Open this select menu</option>--}}
            <option name="role" value="1">Admin</option>
            <option name="role" value="2">Manager</option>
            <option name="role" value="3">User</option>
    </select>

</div>

@csrf
