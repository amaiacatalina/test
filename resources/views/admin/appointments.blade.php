@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row flex-nowrap">
            <div class="col-auto px-0">
                <div id="sidebar" class="collapse collapse-horizontal show border-end">
                    <div id="sidebar-nav" class="list-group border-0 rounded-0 text-sm-start min-vh-100">
                        <a href="{{route('users')}}" class="list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-film"></i> <span>Users</span></a>
                        <a href="{{route('doctors')}}" class="list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-bootstrap"></i> <span>Doctors</span> </a>
                        <a href="{{route('specialties')}}" class="list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-heart"></i> <span>Specialties</span></a>
                        <a href="{{route('appointments')}}" class="{{(request()->is('appointments')) ? 'active' : 'bg-light', 'text-white'}} list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-bricks"></i> <span>Appointments</span></a>

                    </div>
                </div>
            </div>
            <main class="col">
                {{--                <a href="#" data-bs-target="#sidebar" data-bs-toggle="collapse" class="border rounded-3 p-1 text-decoration-none bg-dark"><i class="bi bi-list bi-lg py-2"></i><i class="fas fa-grip-lines"></i></a>--}}
                <div class="page-header pt-3">
                    <h2>Appointments Table</h2>
                </div>
                {{--                <hr>--}}
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover">

                            <tr>
                                <th>Doctor name</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th>User</th>
                                <th colspan="2">Approved</th>
                            </tr>

                            <tr>
                                @foreach($apps as $app)
                                    <td>{{$app->doctor->name}}</td>
                                    <td>{{$app->description}}</td>
                                    <td>{{$app->date}}</td>
                                    <td>{{$app->user->name}}</td>
                                    <td><a class="text-success" href="#"><i class="fas fa-check"></i></a></td>
                                    <td><a class="text-danger" href="#"><i class="far fa-times-circle"></i></i></a></td>
                            </tr>
                            @endforeach

                        </table>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection
