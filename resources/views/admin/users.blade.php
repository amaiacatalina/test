@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row flex-nowrap">
            <div class="col-auto px-0">
                <div id="sidebar" class="collapse collapse-horizontal show border-end">
                    <div id="sidebar-nav" class="list-group border-0 rounded-0 text-sm-start min-vh-100">
                        <a href="{{route('users')}}" class="{{(request()->is('users')) ? 'active' : 'bg-light', 'text-white'}} list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-film"></i> <span>Users</span></a>
                        <a href="{{route('doctors')}}" class="list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-bootstrap"></i> <span>Doctors</span> </a>
                        <a href="{{route('specialties')}}" class="list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-heart"></i> <span>Specialties</span></a>
                        <a href="{{route('appointments')}}" class="list-group-item border-end-0 d-inline-block text-truncate" data-bs-parent="#sidebar"><i class="bi bi-bricks"></i> <span>Appointments</span></a>

                    </div>
                </div>
            </div>
            <main class="col">
{{--                <a href="#" data-bs-target="#sidebar" data-bs-toggle="collapse" class="border rounded-3 p-1 text-decoration-none bg-dark"><i class="bi bi-list bi-lg py-2"></i><i class="fas fa-grip-lines"></i></a>--}}
                <div class="page-header pt-3">
                    <h2>Users Table</h2>
                </div>
{{--                <hr>--}}
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th class="text-center" colspan="2">Edit</th>
                                <th class="text-center" colspan="2">Delete</th>
                            </tr>
                            @foreach($users as $user)

                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>@if($user->role=='1')
                                            <div class="text-sm fg-black a">Admin</div>
                                        @endif
                                        @if($user->role=='2')
                                            <div class="text-sm fg-black a">Manager</div>
                                        @endif
                                        @if($user->role=='3')
                                            <div class="text-sm fg-black a">User</div>

                                        @endif</td>
                                    <td class="text-center" colspan="2"><a href="{{ route('admin.edit', ['user'=>$user])}}"
                                                                           class="btn btn-primary py-0">Edit</a></td>
                                    <td class="text-center" colspan="2">
                                        <form class="form" action="{{ route('admin.destroy', ['user'=>$user]) }}" method="POST">
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger py-0">Delete</button>
                                            @csrf
                                        </form>
                                    </td>
                                    @endforeach

                                </tr>
                        </table>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection

