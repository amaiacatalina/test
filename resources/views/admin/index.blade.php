@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row flex-nowrap">
            <div class="col-auto px-0">
                <div id="sidebar" class="collapse collapse-horizontal show border-end">
                    <div id="sidebar-nav" class="list-group border-0 rounded-0 text-sm-start min-vh-100">
                        <a href="{{route('users')}}" class="list-group-item border-end-0 d-inline-block text-truncate"
                           data-bs-parent="#sidebar"><i class="bi bi-film"></i> <span>Users</span></a>
                        <a href="{{route('doctors')}}" class="list-group-item border-end-0 d-inline-block text-truncate"
                           data-bs-parent="#sidebar"><i class="bi bi-bootstrap"></i> <span>Doctors</span> </a>
                        <a href="{{route('specialties')}}"
                           class="list-group-item border-end-0 d-inline-block text-truncate"
                           data-bs-parent="#sidebar"><i class="bi bi-heart"></i> <span>Specialties</span></a>
                        <a href="{{route('appointments')}}" class="list-group-item border-end-0 d-inline-block text-truncate"
                           data-bs-parent="#sidebar"><i class="bi bi-bricks"></i> <span>Appointments</span></a>

                    </div>
                </div>
            </div>
            <main class="col ps-md-2 pt-2">
                <a href="#" data-bs-target="#sidebar" data-bs-toggle="collapse"
                   class="border rounded-3 p-1 text-decoration-none bg-dark"><i class="bi bi-list bi-lg py-2"></i><i
                        class="fas fa-grip-lines"></i></a>
                <div class="page-header pt-3">
                    <h2>Users Table</h2>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="container-fluid">
                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">Admin
                                            Dashboard</div>
                                        <div class="card-body">
                                            @if (session('status'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session('status') }}
                                                </div>
                                            @endif
                                            <a class="btn btn-primary" href="{{route('admin.create')}}">Add user</a>
                                            <a class="btn btn-primary" href="{{ route('doctors.create')}}">Add
                                                specialist</a>
                                            <a class="btn btn-primary" href="{{ route('spec.create')}}">Add
                                                specialisation</a>
                                            <h2 class="my-3 text-capitalize">Users table</h2>

{{--                                            @include('admin.users')--}}
                                            <h2 class="my-3 text-capitalize">Doctors table</h2>
{{--                                            @include('admin.slide')--}}
                                            <a class="btn btn-success"
                                               href="{{route('appointment.show')}}">Appointments</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection
