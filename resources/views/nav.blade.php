<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a href="{{route('home')}}" class="navbar-brand">Reve<span class="text-primary">MED.</span></a>

        <ul class="navbar-nav ms-auto">
            <!-- Authentication Links -->
            @guest
                @if (Route::has('login'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @endif

                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                            <a class="{{(request()->routeIs('admin')) ? 'disabled' : ''}} dropdown-item"
                               href="{{ route('admin') }}">Dashboard</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
        <!-- Authentication Links -->

        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarContent">
            <ul class="navbar-nav ml-auto pt-3 pt-lg-0">
                {{--                <li class="nav-item active">--}}
                <li class="{{(request()->is('/')) ? 'active' : 'nav-item'}}">
                    <a href="{{route('home')}}" class="nav-link">Home</a>
                </li>
                <li class="{{(request()->routeIs('doctors.index')) ? 'active' : 'nav-item'}}">
                    <a href="{{ route('doctors.index') }}" class="nav-link">About</a>
                </li>
                <li class="{{(request()->is('services')) ? 'active' : 'nav-item'}}">
                    <a href="{{ route('services') }}" class="nav-link">Services</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('spec.index')}}" class="nav-link">Appointments</a>
                </li>
                <li class="nav-item">
                    <a href="blog.html" class="nav-link">News</a>
                </li>
                <li class="nav-item">
{{--                    <a href="{{route('cot')}}" class="nav-link">Contact</a>--}}
                                </li>
            </ul>
        </div>
    </div> <!-- .container -->
</nav>
