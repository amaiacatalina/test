@extends('layouts.app')
@section('content')

    <main>
        <div class="page-section">
            <div class="container">
                <div class="text-center">
                    <h2 class="title-section mb-3">Stay in touch</h2>
                    <p>Just say hello or drop us a line. You can manualy send us email on <a href="mailto:example@mail.com">example@mail.com</a></p>
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-lg-8">
                        <form action="{{route('contact.store')}}" method="POST" class="form-contact">
                            <div class="row">
                                <div class="col-sm-6 py-2">
                                    <label for="name" class="fg-grey">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter name.." value="{{ old('name') }}">
                                    <div>{{$errors->first('name')}}</div>
                                </div>
                                <div class="col-sm-6 py-2">
                                    <label for="email" class="fg-grey">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email address.." value="{{ old('email') }}" >
                                    <div class="text-danger">{{$errors->first('email')}}</div>
                                </div>
                                <div class="col-12 py-2">
                                    <label for="subject" class="fg-grey">Subject</label>
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject.." value="{{ old('subject') }}" >
                                    <div class="text-danger">{{$errors->first('subject')}}</div>
                                </div>
                                <div class="col-12 py-2">
                                    <label for="message" class="fg-grey">Message</label>
                                    <textarea id="message" rows="8" class="form-control" name="message" placeholder="Enter message.." value="{{ old('message') }}" ></textarea>
                                    <div class="text-danger">{{$errors->first('message')}}</div>
                                </div>
                                <div class="col-12 mt-3">
                                    <button type="submit" class="btn btn-primary px-5">Submit</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div> <!-- .container -->
        </div> <!-- .page-section -->

        <div class="maps-container">
            <div id="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2798.5660136848137!2d28.039349015543685!3d45.45840084185146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b6de1498632b01%3A0x284917abdacbddd5!2sStrada%20Alexandru%20cel%20Bun%2022%2C%20Gala%C8%9Bi%20800552!5e0!3m2!1sro!2sro!4v1640247877793!5m2!1sro!2sro" width="1650" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </main>

@endsection
