@component('mail::message')

Thank you for your message
<div>Name: {{ $data['name'] }}</div>
<div>Email: {{ $data['email']}}</div>
<div>Subject: {{ $data['subject'] }}</div>


Message:
{{ $data['message'] }}

@endcomponent
