<?php

namespace Database\Seeders;

use App\Models\Day;
use Illuminate\Database\Seeder;

class WeekDays extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        $days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $start="09:00:00";
        $end="18:00:00";

        foreach ($days as $day)
        {
            Day::query()->create([
                "name" => $day,
                "start"=>$start,
                "end"=>$end
            ]);
        }
    }
}
