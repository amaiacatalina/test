<?php

namespace Database\Seeders;

use App\Models\Spec;
use Illuminate\Database\Seeder;

class Specialisation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        $specs = ["Pediatry", "Ophthalmology", "Family Medicine", "Dermatology", "Cardiology", "Endocrinology"];

        foreach ($specs as $spec){
            Spec::query()->create([
                'name' => $spec
            ]);
        }
    }
}
