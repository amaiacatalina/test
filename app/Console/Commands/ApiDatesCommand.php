<?php

namespace App\Console\Commands;

use App\Events\NewDateCreated;
use App\Models\Date;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ApiDatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Api call for dates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('127.0.0.1:8001/api/apps');
        $apps = json_decode($response->body());
        foreach ($apps as $app) {
            $date = Date::firstOrCreate([
                'date' => $app->date,
                'user_id' => $app->user_id,
                'doctor_id' => $app->doctor_id,
            ]);
            $date->save();

            event(new NewDateCreated($date));
        }
            return Command::SUCCESS;
        }

}
