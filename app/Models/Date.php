<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Date
 *
 * @property $user_id
 * @property int $id
 * @property string $date
 * @property int $doctor_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Doctor $doctor
 * @property-read User $user
 * @method static Builder|Date newModelQuery()
 * @method static Builder|Date newQuery()
 * @method static Builder|Date query()
 * @method static Builder|Date whereCreatedAt($value)
 * @method static Builder|Date whereDate($value)
 * @method static Builder|Date whereDoctorId($value)
 * @method static Builder|Date whereId($value)
 * @method static Builder|Date whereUpdatedAt($value)
 * @method static Builder|Date whereUserId($value)
 * @method static Builder|Date withAll()
 * @mixin Builder
 */
class Date extends Model
{
    /**
     * @var mixed
     */

    /**
     * @var mixed
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }

    public function scopeWithAll($query)
    {
        $query->with('user', 'doctor');
    }

    protected $dispatchesEvents = [
        'saving' => \App\Events\NewDateCreated::class,
    ];
    protected $guarded =[];
    use HasFactory;
}
