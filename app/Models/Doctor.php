<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $guarded = [];
    /**
     * //     * @var array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\Request|mixed|string|null
     */
    use HasFactory;

    protected $casts = [
        "day_id" => "json"
    ];

    protected $appends = [ 'dates'];

    //specify the fact that day_id is json

    public function spec()
    {
        return $this->belongsTo(Spec::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function days()
    {
        return $this->hasMany(Day::class);
    }

    public function appointment(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Date::class);
    }

    public function getDatesAttribute($query)
    {
        return json_encode($this->appointment()->pluck('date'));
    }
    public function scopeWithAll($query)
    {
        $query->with('user');
    }

}
