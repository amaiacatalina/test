<?php

namespace App\Http\Controllers;

use App\Models\Date;
use App\Models\Doctor;
use App\Models\Spec;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AppointmentsController extends Controller
{
    public function index($spec, Request $request)
    {
        $exists = Date::query()->where('doctor_id', $request->doctor_id)
            ->where('date', '=', $request->date)
            ->exists();
        $apps = Date::withAll()->get();
        $user = Auth::id();
        $doctors = Doctor::query()->where('spec_id', $spec)->get();


        return view('appointments.index', compact('doctors', 'user', 'apps', 'exists'));

    }

    public function show()
    {
        $apps = Date::withAll()->get();
        return view('appointments.show', compact('apps'));
    }

}
