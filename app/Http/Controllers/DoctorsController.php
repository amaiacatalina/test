<?php

namespace App\Http\Controllers;

use App\Models\Day;
use App\Models\Doctor;
use App\Models\Spec;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use PhpParser\Comment\Doc;
use function GuzzleHttp\Promise\all;

class DoctorsController extends Controller
{
    public function index()
    {
        $doctors = Doctor::all()->sortBy('created_at');
        return view('doctors.index', compact('doctors'));//the short way
    }

    public function create()
    {
        $days = Day::all();
        $specs = Spec::all();
        $doctor = new Doctor();
        $user = Auth::id();
        return view('doctors.create', compact('days', 'specs', 'doctor', 'user'));
    }

    public function store(Request $request)
    {
        $doctor = Doctor::query()->create($this->validateRequest($request));
        $this->storeImage($doctor);
        return redirect('doctors');
    }

    public function show(Doctor $doctor)
    {
        return view('doctors.show', compact('doctor'));
    }

    public function edit(Doctor $doctor)
    {
        $specs = Spec::all();
        $days = Day::all();
        $user = $doctor->user_id;
        return view('doctors.edit', compact('days', 'specs', 'doctor','user'));
    }

    public function update(Doctor $doctor, Request $request)
    {
        $request->merge(['startdate' => Carbon::parse($request->startdate)]);
        $request->merge(['enddate' => Carbon::parse($request->enddate)]);
        $doctor->update($request->validate([
            'startdate' => 'date|required',
            'enddate' => 'date|required',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'spec_id' => 'required',
            'phone' => 'required',
            'gender' => 'required',]));
        $this->storeImage($doctor);
        return redirect('doctors/' . $doctor->id);
    }

    public function destroy(Doctor $doctor)
    {
        $doctor->delete();
        return redirect('doctors');
    }

    public function validateRequest(Request $request)
    {
        $request->merge(['startdate' => Carbon::parse($request->startdate)]);
        $request->merge(['enddate' => Carbon::parse($request->enddate)]);
        return $request->validate([
            'startdate' => 'date|required',
            'enddate' => 'date|required',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'spec_id' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'user_id' => 'required']);
    }

    private function storeImage($doctor)
    {
        if (request()->has('image')) {
            $doctor->update([
                'image' => request()->image->store('teams', 'public'),
            ]);

            $image = Image::make(public_path('storage/' . $doctor->image))->fit(300, 375);
            $image->save();
        }
    }


}
