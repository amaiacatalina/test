<?php

namespace App\Http\Controllers;

use App\Http\Middleware\User;
use App\Models\Date;
use App\Models\Doctor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ManagerController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $user = Auth::id();
        $doctors = Doctor::all();
        $doctor = Doctor::query()->where('user_id', $user)->get();
        $exists = Doctor::query()->where('user_id', $user)->exists();
        $apps = Date::all();

        return view('manager.index', compact('user', 'doctors', 'exists', 'doctor', 'apps'));
    }

    /**
     * @return Application|Factory|View
     */
    public function edit()
    {
        return view('manager.edit');
    }
}
