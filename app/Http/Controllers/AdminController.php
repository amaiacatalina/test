<?php

namespace App\Http\Controllers;

use App\Models\Date;
use App\Models\Doctor;
use App\Models\Spec;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        $specs=Spec::all();
        $doctors = Doctor::all();
        $users = User::all()->sortBy('name');
        return view('admin.index', compact('users', 'doctors', 'specs'));
    }

    protected function create()
    {
        $user = new User();
        return view('admin.create', compact('user'));
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'role' => 'required'
        ]);

        $user = User::query()->create(request(['name', 'email', 'password', 'role']));
//        dd($user);
        return redirect()->to('/admin');
    }

    public function edit(User $user)
    {
        return view('admin.edit', compact('user'));
    }

    public function update(User $user)
    {
        $user->update(request(['name', 'email', 'password', 'role']));

        return redirect('/admin/' . $user->id);
    }
    public function destroy(User $user)
    {
        $user->delete();

        return redirect('admin');
    }

    public function users(){
        $users=User::all();

        return view('admin.users', compact('users'));
    }
    public function doctors(){
        $doctors=Doctor::all();

        return view('admin.slide', compact('doctors'));
    }
    public function specialties(){
        $specs=Spec::all();

        return view('admin.specialties', compact('specs'));
    }
    public function appointments(){
        $apps=Date::all();
        $specs=Spec::all();
        return view('admin.appointments', compact('apps', 'specs'));
    }
}
