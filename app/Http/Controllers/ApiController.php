<?php

namespace App\Http\Controllers;

use App\Events\CreateAppointmentEvent;
use App\Http\Resources\ApiResource;
use App\Http\Resources\ApiResourceCollection;
use App\Models\Date;
use App\Models\Doctor;
use App\Models\User;
use Facade\FlareClient\Api;
use Facade\FlareClient\Http\Response;
use http\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use PhpParser\Comment\Doc;

class ApiController extends Controller
{
    /**
     * @param Date $appointment
     * @return ApiResource
     */
    public function show(Date $appointment): ApiResource
    {

        return new ApiResource ($appointment);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'email' => 'required',
            'description' => 'required|max:50',
            'date' => 'required',
            'doctor_id' => 'required',
            'user_id' => 'required'
        ]);

        $appointment = $request->all();
        event(new CreateAppointmentEvent($appointment));

        return redirect('appointments');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
