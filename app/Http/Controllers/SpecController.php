<?php

namespace App\Http\Controllers;

use App\Models\Date;
use App\Models\Day;
use App\Models\Doctor;
use App\Models\Spec;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\HigherOrderTapProxy;
use Intervention\Image\Facades\Image;

class SpecController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $doctors = Doctor::all();
        $specs = Spec::all();
        $date = Date::all()->last();

        return view('specs.index', compact('doctors','specs', 'date'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $spec = new Spec();

        return view('specs.create', compact('spec'));
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $spec = Spec::query()->create($this->validateRequest($request));

        $this->storeImage($spec);

        return redirect('appointments');
    }

    /**
     * @param Spec $spec
     * @return Application|Factory|View
     */
    public function show (Spec $spec)
    {
        return view('specs.show', compact('spec'));
    }

    /**
     * @param Spec $spec
     * @return Application|Factory|View
     */
    public function edit(Spec $spec)
    {
        return view('specs.edit', compact('spec'));
    }

    /**
     * @param Spec $spec
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function update(Spec $spec, Request  $request)
    {
        $spec->update($this->validateRequest($request));
        $this->storeImage($spec);

        return redirect('specialisation/' . $spec->id);
    }

    /**
     * @param Spec $spec
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy(Spec $spec)
    {
        $spec->delete();

        return redirect('admin.index');
    }

    /**
     * @param Request $request
     * @return HigherOrderTapProxy|mixed
     */
    public function validateRequest(Request $request)
    {
        return tap($request->validate([
            'name' => 'required|min:3',
        ]), function ($request) {

            if (request()->hasFile('image')){
                request()->validate([
                    'image'=> 'file|image|max:5000'
                ]);
            }
        });
    }

    /**
     * @param Spec $spec
     */
    private function storeImage(Spec $spec)
    {
        if(request()->has('image')){
            $spec->update([
                'image' => request()->image->store('specs','public'),
            ]);

            $image=Image::make(public_path('storage/' . $spec->image))->fit(300, 375);
            $image->save();
        }
    }
}
